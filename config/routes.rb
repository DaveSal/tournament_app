Rails.application.routes.draw do
  root "tournaments#index"

  resources :tournaments, only: [:index, :new, :create, :show]
  resources :tournament_division_results, only: [:create]
end
