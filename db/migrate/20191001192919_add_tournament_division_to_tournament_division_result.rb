class AddTournamentDivisionToTournamentDivisionResult < ActiveRecord::Migration[5.2]
  def change
    add_reference :tournament_division_results, :tournament_division, foreign_key: true
  end
end
