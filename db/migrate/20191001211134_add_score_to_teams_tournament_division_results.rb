class AddScoreToTeamsTournamentDivisionResults < ActiveRecord::Migration[5.2]
  def change
    add_column :teams_tournament_division_results, :score, :integer
  end
end
