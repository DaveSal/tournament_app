class CreateTournamentDivisionResults < ActiveRecord::Migration[5.2]
  def change
    create_table :tournament_division_results do |t|

      t.timestamps
    end
  end
end
