class CreateJoinTableTournamentDivisionsTeams < ActiveRecord::Migration[5.2]
  def change
    create_join_table :tournament_divisions, :teams
  end
end
