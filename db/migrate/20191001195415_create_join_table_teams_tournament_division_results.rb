class CreateJoinTableTeamsTournamentDivisionResults < ActiveRecord::Migration[5.2]
  def change
    create_join_table :teams, :tournament_division_results
  end
end
