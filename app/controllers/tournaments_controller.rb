class TournamentsController < ApplicationController
  def index
    @tournaments = Tournament.all.order(created_at: :DESC)
  end

  def new
    @tournament = Tournament.new
    SOFT_SETTINGS["teams_number"].times { @tournament.teams.build }
  end

  def create
    @tournament = Tournament.new(tournaments_params)

    if @tournament.save
      flash[:success] = "Tournament successfully created"
     redirect_to tournament_path(@tournament)
    else
     render :new
    end
  end

  def show
    @tournament = Tournament.find(params[:id])
  end

  private

    def tournaments_params
      params.require(:tournament).permit(:title, teams_attributes: [:id, :title])
    end
end
