class TournamentDivisionResultsController < ApplicationController
  def create
    @tournament_division = TournamentDivision.find_by(id: params[:tournament_division_id])

    if TournamentDivisionResult.create(tournament_division: @tournament_division)
      flash[:suceess] = "Results generated successfully"
    else
      flash[:suceess] = "Results were not generated due to some reason"
    end

    redirect_to tournament_path(@tournament_division.tournament)
  end
end
