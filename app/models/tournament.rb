class Tournament < ApplicationRecord
  include TournamentCallbacks

  has_and_belongs_to_many :teams
  has_many :tournament_divisions

  accepts_nested_attributes_for :teams

  def first_division
    tournament_divisions.order(:id).first
  end

  def second_division
    tournament_divisions.order(:id).second
  end

  def division_results(number)
    send("#{number}_division").tournament_division_result
                              &.teams
                              &.joins(:teams_tournament_division_results)
                              &.order("teams_tournament_division_results.score DESC")
  end

  def play_off
    teams.joins(:teams_tournament_division_results).order(score: :DESC)
  end

  def winner
  end
end
