class TournamentDivisionResult < ApplicationRecord
  module TournamentDivisionResultCallbacks
    extend ActiveSupport::Concern

    included do
      after_save :generate_results
    end

    private

      def generate_results
        division_teams = tournament_division.teams

        teams_rating_hash = division_teams.each_with_object(Hash.new(0)) do |t, hash|
          division_teams.each do |i|
            next if i == t
            hash[[t, i].sample] += 1
          end
        end.sort_by { |k,v| -v }.to_h

        teams_rating_hash.first(4).each do |team,score|
          TeamsTournamentDivisionResult.create(team: team, score: score, tournament_division_result_id: self.id)
        end
      end
  end
end