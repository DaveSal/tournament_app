class Tournament < ApplicationRecord
  module TournamentCallbacks
    extend ActiveSupport::Concern

    included do
      after_save :create_tournament_divisions
    end

    private

      def create_tournament_divisions
        teams.shuffle.each_slice(SOFT_SETTINGS["teams_number"] / SOFT_SETTINGS["divisions_number"]) do |division_teams|
          TournamentDivision.create(teams: division_teams, tournament: self)
        end
      end
  end
end