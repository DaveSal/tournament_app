class TournamentDivisionResult < ApplicationRecord
  include TournamentDivisionResultCallbacks

  has_and_belongs_to_many :teams
  belongs_to :tournament_division
  has_one :teams_tournament_division_result
end
