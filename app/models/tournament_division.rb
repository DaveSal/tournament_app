class TournamentDivision < ApplicationRecord
  belongs_to :tournament
  has_and_belongs_to_many :teams
  has_one :tournament_division_result
end
