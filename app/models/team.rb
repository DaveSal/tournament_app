class Team < ApplicationRecord
  has_and_belongs_to_many :tournaments
  has_and_belongs_to_many :tournament_divisions
  has_and_belongs_to_many :tournament_division_results
  has_one :teams_tournament_division_result

  def score
    teams_tournament_division_result.score
  end
end
