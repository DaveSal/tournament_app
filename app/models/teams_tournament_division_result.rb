class TeamsTournamentDivisionResult < ApplicationRecord
  self.table_name = "teams_tournament_division_results"
  default_scope { order(team_id: :desc) }

  belongs_to :team
  belongs_to :tournament_division_result
end